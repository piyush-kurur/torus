{-|

Top level module that exposes the core language.

-}
module Torus.Core
       ( module Torus.Core.Pretty
       ) where

import Torus.Core.Pretty
